<?php

declare(strict_types=1);

namespace App\Entity\Model;

final class Post
{
    public string $title;
    public string $summary;
    public string $authorName;
    public string $slug;

    /**
     * @var array<PostComment>
     */
    public array $comments = [];

    public ?\DateTime $publishedAt;
}
