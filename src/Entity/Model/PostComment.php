<?php

declare(strict_types=1);

namespace App\Entity\Model;

final class PostComment
{
    public string $content;
    public string $authorName;
}
